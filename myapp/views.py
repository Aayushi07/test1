from django.shortcuts import render
from rest_framework import serializers
from.models import Student
from.serializer import StudentSerializer
from django. http import HttpResponse,JsonResponse
from rest_framework . renderers import JSONRenderer




# Create your views here.
## data fetch in front end
## models instance single data

def Student_details(request,pk):
    stu = Student.objects.get(id = pk)
    serializer = StudentSerializer(stu)
    # json_data = JSONRenderer().render(serializer.data)
    # return HttpResponse(json_data,content_type = 'application/json').
    return JsonResponse(serializer.data)

## qweryset multiple data

def Student_list(request):
    queryset = Student.objects.all()
    serializer = StudentSerializer(queryset,many=True)
    json_data = JSONRenderer().render(serializer.data)
    return HttpResponse(json_data,content_type = 'application/json')

## function based api view ## crud operation
from rest_framework.decorators import api_view
from rest_framework.response import Response

## example
# @api_view()   ## bydeafult get method is inserted
# def hello(request):
#     if request.method == 'GET':
#         return Response({'msg':'hyeee'})

#     if request.method == 'POST':
#         print(request.data)
        
#         return Response({'msg':'this is post request','data':request.data})


@api_view(['GET','POST','PUT','PATCH','DELETE'])

def student_api(request,pk=None):
    if request.method == 'GET':
        id = request.data.get('id')
        if id is not None:
            stu = Student.objects.get(id=id)
            serializer = StudentSerializer(stu)
            return Response(serializer.data)
        stu = Student.objects.all()
        serializer = StudentSerializer(stu, many=True)
        return Response(serializer.data)

    if request.method == 'POST':
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':"data is created",'data':request.data})
        return Response(serializer.errors)

    

    if requests.method == 'PUT':
        id = pk
        stu = Student.objects.get(id=id)
        serializers = StudentSerializer(stu, data=requests.data)
        if serializers.is_valid():
            serializers.save()
            return Response({'msg':' completly update sucesfully'})
        return Response(serializers.errors,status=status.HTTP_400_BAD_REQUEST)

    if requests.method == 'PATCH':
        id = pk
        stu = Student.objects.get(id=id)
        serializers = StudentSerializer(stu, data=requests.data,partial=True)
        if serializers.is_valid():
            serializers.save()
            return Response({'msg':'update sucesfully'})
        return Response(serializers.errors,status=status.HTTP_400_BAD_REQUEST)




    if requests.method == "DELETE":
        id = pk
        stu = Student.objects.get(id=id)
        stu.delete()
        return Response({'msg': 'delete sucessfully'})


## class based api view
from rest_framework.views import APIView

class StudentApi(APIView):
    def get(self,request,pk=None,format=None):
        id = pk
        if id is not None:
            stu = Student.objects.get(id=id)
            serializer = StudentSerializer(stu)
            return Response(serializer.data)
        stu = Student.objects.all()
        serializer = StudentSerializer(stu,many=True)
        return Response(serializer.data)

    def post(self,request,format=None):
        serializer= StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"data is created","data":request.data})

    def put(self,request,pk,formate=None):
        id = pk
        stu = Student.objects.get(pk=id)
        serializer = StudentSerializer(stu,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"data is update","data":request.data})
            return Response(serializers.errors,status=status.HTTP_400_BAD_REQUEST)


    def patch(self,request,pk,formate=None):
        id = pk
        stu = Student.objects.get(pk=id)
        serializers = StudentSerializer(stu, data=requests.data,partial=True)
        if serializers.is_valid():
            serializers.save()
            return Response({'msg':'update sucesfully'})
        return Response(serializers.errors,status=status.HTTP_400_BAD_REQUEST)

    def delete(self,request,pk,formate=None):
        id = pk
        stu = Student.objects.get(pk=id)
        stu.delete()
        return Response({"msg":"successfully deleted"})


#### generic view

from rest_framework.mixins import ListModelMixin,CreateModelMixin,UpdateModelMixin,RetrieveModelMixin,DestroyModelMixin
from rest_framework.generics import GenericAPIView

class StudentList(GenericAPIView,ListModelMixin):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    def get(self,request,*arg,**kwargs):
        return self.list(request,*arg,**kwargs)

class StudentCreate(GenericAPIView,CreateModelMixin):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    def post(self,request,*arg,**kwargs):
        return self.create(request,*arg,**kwargs)

class StudentRetrieve(GenericAPIView,RetrieveModelMixin):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    def get(self,request,*arg,**kwargs):
        return self.retrieve(request,*arg,**kwargs)


 
class StudentUpdate(GenericAPIView,UpdateModelMixin):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    def put(self,request,*arg,**kwargs):
        return self.update(request,*arg,**kwargs)

class StudentDestroy(GenericAPIView,DestroyModelMixin):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    def delete(self,request,*arg,**kwargs):
        return self.destroy(request,*arg,**kwargs)

#### concrete api view
from rest_framework.generics import ListAPIView,CreateAPIView,RetrieveAPIView,UpdateAPIView,DestroyAPIView

class StudentList1(ListAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentCreate1(CreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentRetrieve1(RetrieveAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentUpdate1(UpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentDestroy1(DestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

from rest_framework.generics import ListCreateAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView,RetrieveUpdateDestroyAPIView

class StudentListCreate(ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentRetrieveUpdate(RetrieveUpdateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentRetrieveDestroy(RetrieveDestroyAPIView,):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentRetrieveUpdateDestroy(RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


##### viewset not use many time urls becasue we use routers and in this class we reapet logics in single class
### viewsetclass this is provide list() and create () not provide get and post
# list()   get all record
# retrieve ()  get single record
# create() post and create record and insert
#update() update record completelty
#partially_update() update record partially
# destroy () delete record
from rest_framework import viewsets
class StudentViewSet(viewsets.ViewSet):
    def list(self,request):
        print('........List........')
        print("Basename:",self.basename)
        print("Action:",self.action)
        print("Detail:",self.detail)
        print("Suffix:",self.suffix)
        print("Name:",self.name)
        print("Description:",self.description)
        stu=Student.objects.all()
        serializer = StudentSerializer(stu,many=True)
        return Response(serializer.data)

    def retrieve(self,request,pk=None):
        id = pk
        if id is not None:
            stu = Student.objects.get(id=id)
            serializer = StudentSerializer(stu)
            return Response(serializer.data)

    def create(self,request):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"data is createted","data":request.daat})
        return Response(serializer.errors)

    def update(self,request,pk=None):
        id = pk
        if id is not None:
            stu = Student.objects.get(pk=id)
            serializer = StudentSerializer(stu,data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"msg":"data is updated",'data':request.data})
            return Response(serializer.errors)

    def partial_update(self,request,pk):
        id = pk
        
        stu = Student.objects.get(pk=id)
        serializer = StudentSerializer(stu,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"msg":"data is updated",'data':request.data})
        return Response(serializer.errors)

    def destroy(self,request,pk):
        id = pk
        stu = Student.objects.get(pk=id)
        stu.delete()
        return Response({"msg":"successfully deleted"})

## model view set all methods bydefault
## AllowAny ka use tab krte hai jab koi bhi us api ko use kr skta hai [AllowAny]
## IsAdminUser    use specific people jis user ka isstaff true hoga admin panel m vhi user use kr apyega
from rest_framework.authentication import BasicAuthentication,SessionAuthentication
from rest_framework.permissions import IsAuthenticated
class AdminViewSet(viewsets.ModelViewSet):
    queryset=Student.objects.all()
    serializer_class=StudentSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [BasicAuthentication]



## readonlymodelviewset only show retrieve data

class AdminReadOnlyModelViewSet(viewsets.ReadOnlyModelViewSet):
    queryset=Student.objects.all()
    serializer_class=StudentSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication]






 
 


   