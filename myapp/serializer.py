from rest_framework import serializers
from.models import Student


# class StudentSerializer(serializers.Serializer):
    
#     name =serializers.CharField(max_length=20)
#     city =serializers.CharField(max_length=20)
#     roll =serializers.IntegerField()

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['name','city','roll']