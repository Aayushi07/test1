from django.contrib import admin
from django.urls import path,include
from.import views
# from rest_framework.routers import DefaultRouter

# router = DefaultRouter()
# router.register('student',views.StudentViewSet,basename='student'),


urlpatterns = [
    path('student/<int:pk>',views.Student_details,name='student'),
    path('student_list',views.Student_list,name='student_list'),
    #path('hello',views.hello,name='hello'),
    path('student_api/<int:pk>',views.student_api,name='student_api'),
    path('user/<int:pk>',views.StudentApi.as_view(),name='user'),
    path('user',views.StudentApi.as_view(),name='user'),
    path('studentlist',views.StudentList.as_view(),name='studentlist'),
    path('studentcreate',views.StudentCreate.as_view(),name='studentcreate'),
    path('studentretrieve/<int:pk>',views.StudentRetrieve.as_view(),name='studentretrieve'),
    path('studentupdate/<int:pk>',views.StudentUpdate.as_view(),name='studentupdate'),
    path('studentdestroy/<int:pk>',views.StudentDestroy.as_view(),name='studentdestroy'),
    path('studentlist1',views.StudentList1.as_view(),name='studentlist1'),
    path('studentcreate1',views.StudentCreate1.as_view(),name='studentcreate1'),
    path('studentretrieve1/<int:pk>',views.StudentRetrieve1.as_view(),name='studentretrieve1'),
    path('studentupdate1/<int:pk>',views.StudentUpdate1.as_view(),name='studentupdate1'),
    path('studentdestroy1/<int:pk>',views.StudentDestroy1.as_view(),name='studentdestroy1'),
    path('studentlistcreate',views.StudentListCreate.as_view(),name='studentlistcreate'),
    path('studentretrieveupdate/<int:pk>',views.StudentRetrieveUpdate.as_view(),name='studentlistcreate'),
    path('studentretrievedestroy/<int:pk>',views.StudentRetrieveDestroy.as_view(),name='studentretrievedestroy'),
    path('studentretrieveupdatedestroy/<int:pk>',views.StudentRetrieveUpdateDestroy.as_view(),name='studentretrieveupdatedestroy'),
    #path('api',include(router.urls)),
]
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('student',views.StudentViewSet,basename='student'),
router.register('student1',views.AdminViewSet,basename='student1'),
router.register('student2',views.AdminReadOnlyModelViewSet,basename='student2'),

##router.register('student/<int:pk>',views.StudentViewSet,basename='student')## no need int:pk in routers

urlpatterns = [
    path('',include(router.urls)),
    #path('auth',include('rest_framework.urls')),
]